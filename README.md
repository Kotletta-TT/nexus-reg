###***AutoSettings Nexus Registry***

Simple individual settings (nuget, npm - local repository and docker-proxy and registry) 

*Active users for commit repos:* 
- npm:npm 
- nuget:nuget 
- docker:docker

*AutoCreate SSL-certificate*


First execute
```sh
./start.sh localhost admin_pass
```

- localhost - enter domainname to create certificate 
- admin_pass - enter your password


Other times sample start to docker-compose

```sh
docker-compose up -d
````