#!/bin/sh


#Global variable to create dir, for containers, and domain name to create certificate
NEXUS_DATA_DIR=./nexus-data
NGINX_DATA_DIR=./nginx
DOMAIN_NAME=$1
ADMIN_PASS=$2

mkdir -p $NEXUS_DATA_DIR
sudo -u root chown -R 200 $NEXUS_DATA_DIR
mkdir -p $NGINX_DATA_DIR/ssl && mkdir -p $NGINX_DATA_DIR/conf

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj '/CN='$DOMAIN_NAME -keyout $NGINX_DATA_DIR/ssl/nginx.key -out $NGINX_DATA_DIR/ssl/nginx.crt
cp registry.conf $NGINX_DATA_DIR/conf

docker-compose up -d
sleep 60
#Check first start containers and change password
if [ -f "$NEXUS_DATA_DIR/admin.password" ]; then
    pass=$(head -n 1 $NEXUS_DATA_DIR/admin.password)
    curl --cacert $NGINX_DATA_DIR/ssl/nginx.crt -I -X GET "https://localhost/"
    sleep 10
    #change_password
    curl -u admin:$pass --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X PUT "https://localhost/service/rest/beta/security/users/admin/change-password" -H "accept: application/json" -H "Content-Type: text/plain" -d "$ADMIN_PASS"
    #create npm user
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/security/users" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"userId\": \"npm\", \"firstName\": \"npm\", \"lastName\": \"npm\", \"emailAddress\": \"npm@npm.ru\", \"password\": \"npm\", \"status\": \"active\", \"roles\": [ \"nx-admin\" ]}"
    #create docker user
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/security/users" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"userId\": \"docker\", \"firstName\": \"docker\", \"lastName\": \"docker\", \"emailAddress\": \"docker@docker.ru\", \"password\": \"docker\", \"status\": \"active\", \"roles\": [ \"nx-admin\" ]}"
    #create nuget user
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/security/users" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"userId\": \"nuget\", \"firstName\": \"nuget\", \"lastName\": \"nuget\", \"emailAddress\": \"nuget@nuget.ru\", \"password\": \"nuget\", \"status\": \"active\", \"roles\": [ \"nx-admin\" ]}"
    #delete all default repos
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X DELETE "https://localhost/service/rest/beta/repositories/nuget.org-proxy" -H "accept: application/json"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X DELETE "https://localhost/service/rest/beta/repositories/maven-central" -H "accept: application/json"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X DELETE "https://localhost/service/rest/beta/repositories/maven-public" -H "accept: application/json"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X DELETE "https://localhost/service/rest/beta/repositories/maven-releases" -H "accept: application/json"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X DELETE "https://localhost/service/rest/beta/repositories/maven-snapshots" -H "accept: application/json"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X DELETE "https://localhost/service/rest/beta/repositories/nuget-hosted" -H "accept: application/json"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X DELETE "https://localhost/service/rest/beta/repositories/nuget-group" -H "accept: application/json"
    #create blobstores
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/blobstores/file" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"softQuota\": null, \"name\": \"nuget\", \"path\": \"/nexus-data/blobs/nuget\"}"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/blobstores/file" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"softQuota\": null, \"name\": \"npm\", \"path\": \"/nexus-data/blobs/npm\"}"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/blobstores/file" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"softQuota\": null, \"name\": \"docker\", \"path\": \"/nexus-data/blobs/docker\"}"
    #create repos
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/repositories/nuget/hosted" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"nuget\", \"online\": true, \"storage\": { \"blobStoreName\": \"nuget\", \"strictContentTypeValidation\": true, \"writePolicy\": \"ALLOW_ONCE\" }, \"cleanup\": null}"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/repositories/npm/hosted" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"npm\", \"online\": true, \"storage\": { \"blobStoreName\": \"npm\", \"strictContentTypeValidation\": true, \"writePolicy\": \"ALLOW_ONCE\" }, \"cleanup\": null}"
    #ON realms
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X PUT "https://localhost/service/rest/beta/security/realms/active" -H "accept: application/json" -H "Content-Type: application/json" -d "[ \"NexusAuthenticatingRealm\", \"NexusAuthorizingRealm\", \"NpmToken\", \"NuGetApiKey\", \"DockerToken\"]"
    #create docker-repo, proxy
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/repositories/docker/hosted" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"docker-hosted\", \"online\": true, \"storage\": { \"blobStoreName\": \"docker\", \"strictContentTypeValidation\": true, \"writePolicy\": \"ALLOW\" }, \"cleanup\": null, \"docker\": { \"v1Enabled\": true, \"forceBasicAuth\": false, \"httpPort\": 8083, \"httpsPort\": null }}"
    curl -u admin:$ADMIN_PASS --cacert $NGINX_DATA_DIR/ssl/nginx.crt -X POST "https://localhost/service/rest/beta/repositories/conan/proxy" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"docker-hub\", \"online\": true, \"storage\": { \"blobStoreName\": \"docker\", \"strictContentTypeValidation\": true }, \"cleanup\": null, \"proxy\": { \"remoteUrl\": \"https://registry-1.docker.io\", \"contentMaxAge\": 1440, \"metadataMaxAge\": 1440 }, \"negativeCache\": { \"enabled\": false, \"timeToLive\": 1440 }, \"httpClient\": { \"blocked\": false, \"autoBlock\": true, \"connection\": { \"retries\": null, \"userAgentSuffix\": null, \"timeout\": null, \"enableCircularRedirects\": false, \"enableCookies\": false }, \"authentication\": null, \"routingRule\": \"string\", \"docker\": { \"v1Enabled\": true, \"forceBasicAuth\": true, \"httpPort\": 8082, \"httpsPort\": 8083 }, \"dockerProxy\": { \"indexType\": \"HUB\", \"indexUrl\": \"string\" }}}"
fi
